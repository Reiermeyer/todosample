package playgrounds.reiermeyer.todosample.Database;

import android.provider.BaseColumns;

/**
 * Created by Propaganda Minister on 13.07.2017.
 */

public class TodoContract {
    public static final String DB_NAME = "todolist.db";
    public static final int DB_VERSION = 1;

    public class ToDoEntry implements BaseColumns {
        public static final String TABLE = "todos";
        public static final String COL_ID = "id";
        public static final String COL_TODO_TITLE = "title";
        public static final String COL_TODO_ISDONE = "isdone";
        public static final String COL_TODO_DATE = "date";
    }
}
