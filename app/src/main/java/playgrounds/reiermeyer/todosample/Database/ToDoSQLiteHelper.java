package playgrounds.reiermeyer.todosample.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Propaganda Minister on 13.07.2017.
 */

public class ToDoSQLiteHelper extends SQLiteOpenHelper {

    public ToDoSQLiteHelper(Context context){
        super(context, TodoContract.DB_NAME, null, TodoContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TodoContract.ToDoEntry.TABLE + "( " +
                TodoContract.ToDoEntry.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                TodoContract.ToDoEntry.COL_TODO_TITLE + " TEXT NOT NULL , " +
                TodoContract.ToDoEntry.COL_TODO_ISDONE + " INTEGER , " +
                TodoContract.ToDoEntry.COL_TODO_DATE + " TEXT );";
        try {
            db.execSQL(createTable);
        }catch (Exception ex){
            String s = ex.getMessage();
            String s3 = ex.toString();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}