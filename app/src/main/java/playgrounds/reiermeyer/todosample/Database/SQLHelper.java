package playgrounds.reiermeyer.todosample.Database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import playgrounds.reiermeyer.todosample.Models.ToDo;

/**
 * Created by Propaganda Minister on 13.07.2017.
 */

public class SQLHelper {

    public static boolean InsertTodo(SQLiteDatabase db, ToDo item) {

        int isDone = item.isDone() ? 1 : 0;

        String query = "INSERT INTO " + TodoContract.ToDoEntry.TABLE +
                "(" + TodoContract.ToDoEntry.COL_TODO_TITLE +
                ", " + TodoContract.ToDoEntry.COL_TODO_ISDONE +
                ", " + TodoContract.ToDoEntry.COL_TODO_DATE + ")" +
                "VALUES( '" + item.getToDo() +
                "', " + isDone +
                ", '" + item.getCreationDate().getMonth() + "/" + item.getCreationDate().getDay() + "/" + item.getCreationDate().getYear() + "');";

        db.execSQL(query);

        return false;
    }

    public static void UpdateToDo(SQLiteDatabase db, ToDo item){
        int isDone = item.isDone() ? 1 : 0;
        try {
            String query = "UPDATE " + TodoContract.ToDoEntry.TABLE + " SET " +
                        TodoContract.ToDoEntry.COL_TODO_ISDONE + " = " + isDone +
                    " WHERE " + TodoContract.ToDoEntry.COL_ID +  " = '" + item.getId() + "';";

            db.execSQL(query);
        }catch(Exception ex){
            String s = ex.getMessage();
        }



//        UPDATE table_name
//        SET column1 = value1, column2 = value2...., columnN = valueN
//        WHERE [condition];

    }

    public static List<ToDo> GetTodos(SQLiteDatabase db, boolean done) {

        String[] columns = {
                        TodoContract.ToDoEntry.COL_ID,
                        TodoContract.ToDoEntry.COL_TODO_TITLE,
                        TodoContract.ToDoEntry.COL_TODO_ISDONE,
                        TodoContract.ToDoEntry.COL_TODO_DATE};

        String whereClause = TodoContract.ToDoEntry.COL_TODO_ISDONE + " = ?";
        String[] whereArgs = new String[]{ done ? "1" : "0"};

        List<ToDo> resultList = new ArrayList<>();

        try {
            Cursor cursor = db.query(TodoContract.ToDoEntry.TABLE, columns, whereClause, whereArgs, null, null, null);
            while (cursor.moveToNext()) {

                DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

                int id = cursor.getInt(0);
                String title = cursor.getString(1);
                boolean isDone = cursor.getInt(2) > 0 ? true : false;
                Date date = df.parse(cursor.getString(3));

                resultList.add(new ToDo(title, isDone, date, id));

            }
        } catch (Exception ex) {
            String s = ex.getMessage();
            String s2 = ex.toString();
        }

        ToDo[] resultArray = new ToDo[resultList.size()];
        resultList.toArray(resultArray);

        return  resultList;
    }

}
