package playgrounds.reiermeyer.todosample.Events;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Propaganda Minister on 15.07.2017.
 */

public class EventDispatcher implements  IEventDispatcher {

    protected ArrayList<Listener> listenerList = new ArrayList<>();


    @Override
    public void addEventListener(IEventHandler handler) {
        Listener listener = new Listener(handler);
        listenerList.add(listener);

    }

    @Override
    public void removeEventListener(String type) {

    }

    @Override
    public void dispatchEvent(DataUpdatedEvent event) {
        for(Iterator<Listener> iterator = listenerList.iterator();iterator.hasNext();){
            Listener listener = (Listener) iterator.next();
            IEventHandler eventHandler = listener.getHandler();
            eventHandler.callback(event);
        }
    }

    @Override
    public Boolean hasEventListener(String type) {
        return null;
    }

    @Override
    public void removeAllListeners() {

    }
}
