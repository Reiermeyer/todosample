package playgrounds.reiermeyer.todosample.Events;

/**
 * Created by Propaganda Minister on 15.07.2017.
 */

public interface IEventDispatcher {
    public void addEventListener(IEventHandler cbInterface);
    public void removeEventListener(String type);
    public void dispatchEvent(DataUpdatedEvent event);
    public Boolean hasEventListener(String type);
    public void removeAllListeners();
}
