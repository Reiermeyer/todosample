package playgrounds.reiermeyer.todosample.Events;

/**
 * Created by Propaganda Minister on 15.07.2017.
 */

public interface IEventHandler {
    public void callback(DataUpdatedEvent event);
}
