package playgrounds.reiermeyer.todosample.Events;

/**
 * Created by Propaganda Minister on 15.07.2017.
 */

public class Listener {
    private String type;
    private IEventHandler handler;

    public Listener(IEventHandler handler){
        this.handler = handler;
    }

    public IEventHandler getHandler(){
        return this.handler;
    }
}
