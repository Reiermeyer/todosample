package playgrounds.reiermeyer.todosample;

import android.animation.ObjectAnimator;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.Date;
import java.util.List;

import playgrounds.reiermeyer.todosample.Adapters.TodoListAdapter;
import playgrounds.reiermeyer.todosample.Database.SQLHelper;
import playgrounds.reiermeyer.todosample.Database.ToDoSQLiteHelper;
import playgrounds.reiermeyer.todosample.Events.DataUpdatedEvent;
import playgrounds.reiermeyer.todosample.Events.EventDispatcher;
import playgrounds.reiermeyer.todosample.Events.IEventHandler;
import playgrounds.reiermeyer.todosample.Models.ToDo;

/**
 * Created by Propaganda Minister on 15.07.2017.
 */

public class DoneListFragment extends Fragment implements IEventHandler{

    TodoListAdapter mAdapter;
    List<ToDo> mItems;
    ListView mListView;
    SQLiteDatabase mDataBase;
    public EventDispatcher mDispatcher;
    ObjectAnimator mAnimator;

    View.OnClickListener onCheckBoxClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateSelectedItem(view);
        }

        private void updateSelectedItem(View view){
            ToDo item = mItems.get((int)view.getTag());
            item.setDone(false);
            SQLHelper.UpdateToDo(mDataBase, item);
            mDispatcher.dispatchEvent(new DataUpdatedEvent());
        }
    };

    public DoneListFragment() {

    }

    public static DoneListFragment newInstance() {
        return new DoneListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ToDoSQLiteHelper helper = new ToDoSQLiteHelper(getContext());
        mDataBase = helper.getWritableDatabase();
        mItems = loadItems();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mListView = (ListView) rootView.findViewById(R.id.lvTodos);
        mAdapter = new TodoListAdapter(this.getContext(), 0, mItems, onCheckBoxClick);
        mListView.setAdapter(mAdapter);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);

        return rootView;
    }

    private List<ToDo> loadItems(){
        return SQLHelper.GetTodos(mDataBase, true);
    }

    private void refreshList(){
        mItems.clear();
        mItems.addAll(loadItems());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void callback(DataUpdatedEvent event) {
        refreshList();
    }
}
