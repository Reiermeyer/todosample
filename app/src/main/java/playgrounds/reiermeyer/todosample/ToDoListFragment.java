package playgrounds.reiermeyer.todosample;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import java.util.Date;
import java.util.List;

import playgrounds.reiermeyer.todosample.Adapters.TodoListAdapter;
import playgrounds.reiermeyer.todosample.Database.SQLHelper;
import playgrounds.reiermeyer.todosample.Database.ToDoSQLiteHelper;
import playgrounds.reiermeyer.todosample.Events.DataUpdatedEvent;
import playgrounds.reiermeyer.todosample.Events.EventDispatcher;
import playgrounds.reiermeyer.todosample.Events.IEventHandler;
import playgrounds.reiermeyer.todosample.Models.ToDo;

/**
 * Created by Propaganda Minister on 14.07.2017.
 */

public class ToDoListFragment extends Fragment implements IEventHandler {

    List<ToDo> mItems;
    ListView mListView;
    SQLiteDatabase mDataBase;
    TodoListAdapter mAdapter;
    public EventDispatcher mDispatcher;

    View.OnClickListener onCheckBoxClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           updateSelectedItem(view);
        }

        private void updateSelectedItem(View view){
            ToDo item = mItems.get((int)view.getTag());
            item.setDone(true);
            SQLHelper.UpdateToDo(mDataBase, item);
            mDispatcher.dispatchEvent(new DataUpdatedEvent());
        }
    };

    View.OnClickListener onFloatingActionButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           openDialog();
        }

        private void openDialog(){
            final ToDo item = new ToDo("", false, new Date());
            final EditText taskEditText = new EditText(getContext());

            AlertDialog dialog = new AlertDialog.Builder(getContext())
                    .setTitle("ToDo hinzufügen")
                    .setMessage("Was hast du noch zu erledigen ?")
                    .setView(taskEditText)
                    .setPositiveButton("Hinzufügen", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            item.setToDo(String.valueOf(taskEditText.getText()));

                            if(item.getToDo().length() > 0){
                                SQLHelper.InsertTodo(mDataBase, item);
                                refreshList();
                            }
                        }
                    })
                    .setNegativeButton("Abbrechen", null)
                    .create();
            dialog.show();
        }
    };

    public ToDoListFragment() {

    }

    public static ToDoListFragment newInstance() {
        return new ToDoListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ToDoSQLiteHelper helper = new ToDoSQLiteHelper(getContext());
        mDataBase = helper.getWritableDatabase();
        mItems = loadItems();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mListView = (ListView) rootView.findViewById(R.id.lvTodos);
        mAdapter = new TodoListAdapter(this.getContext(), 0, mItems, onCheckBoxClick);
        mListView.setAdapter(mAdapter);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(onFloatingActionButtonClick);

        return rootView;
    }

    private List<ToDo> loadItems(){
        return SQLHelper.GetTodos(mDataBase, false);
    }

    private void refreshList(){
        mItems.clear();
        mItems.addAll(loadItems());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void callback(DataUpdatedEvent event) {
        refreshList();
    }
}
