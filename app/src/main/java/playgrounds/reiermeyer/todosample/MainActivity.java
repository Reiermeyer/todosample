package playgrounds.reiermeyer.todosample;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import playgrounds.reiermeyer.todosample.Events.EventDispatcher;
import playgrounds.reiermeyer.todosample.Events.Listener;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    public EventDispatcher mEventDispatcher;

    public ToDoListFragment mTodoListFragment;
    public DoneListFragment mDoneListFragment;

    public Listener mToDoListender;
    public Listener mDoneListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

    private void initialize(){
        setContentView(R.layout.activity_main);

        mTodoListFragment = ToDoListFragment.newInstance();
        mDoneListFragment = DoneListFragment.newInstance();

        //Event Listener erstellen
        mToDoListender = new Listener(mTodoListFragment);
        mDoneListener = new Listener(mDoneListFragment);

        //Dispatcher erstellen und Listner hinzufügen
        mEventDispatcher = new EventDispatcher();
        mEventDispatcher.addEventListener(mTodoListFragment);
        mEventDispatcher.addEventListener(mDoneListFragment);

        //Dispatcher an die Fragments übergeben, damit diese auch Events senden können
        mTodoListFragment.mDispatcher = mEventDispatcher;
        mDoneListFragment.mDispatcher = mEventDispatcher;

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                mTodoListFragment, mDoneListFragment);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }
}

    class SectionsPagerAdapter extends FragmentPagerAdapter {

        public ToDoListFragment mTodoListFragment;
        public DoneListFragment mDoneListFragment;

        public SectionsPagerAdapter(FragmentManager fm, ToDoListFragment toDoListFragment,
                                    DoneListFragment doneListFragment) {
            super(fm);
            this.mTodoListFragment = toDoListFragment;
            this.mDoneListFragment = doneListFragment;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0 : return mTodoListFragment;
                case 1 : return mDoneListFragment;
            }

            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public String getPageTitle(int position) {
            switch (position){
                case 0 : return  "ToDo";
                case 1 : return  "Done";
            }
            return  "";
        }
    }
