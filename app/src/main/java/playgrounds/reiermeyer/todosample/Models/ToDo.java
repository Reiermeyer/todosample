package playgrounds.reiermeyer.todosample.Models;

import android.service.quicksettings.Tile;

import java.util.Date;

/**
 * Created by Propaganda Minister on 13.07.2017.
 */

public class ToDo {
    private int Id;
    private String ToDo;
    private boolean Done;
    private Date  CreationDate;

    public ToDo(String Title, boolean IsDone, Date CreationDate){
        this.ToDo = Title;
        this.Done = IsDone;
        this.CreationDate = CreationDate;
    }

    public ToDo(String Title, boolean IsDone, Date CreationDate, int Id){
        this.ToDo = Title;
        this.Done = IsDone;
        this.CreationDate = CreationDate;
        this.Id = Id;
    }

    public Date getCreationDate() {
        return CreationDate;
    }

    public void setCreationDate(Date creationDate) {
        CreationDate = creationDate;
    }

    public String getToDo() {
        return ToDo;
    }

    public void setToDo(String toDo) {
        ToDo = toDo;
    }

    public boolean isDone() {
        return Done;
    }

    public void setDone(boolean done) {
        Done = done;
    }

    public int getId() {
        return Id;
    }
}
