package playgrounds.reiermeyer.todosample.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import playgrounds.reiermeyer.todosample.Models.ToDo;
import playgrounds.reiermeyer.todosample.R;

/**
 * Created by Propaganda Minister on 13.07.2017.
 */

public class TodoListAdapter extends ArrayAdapter<ToDo> {
    private Context context;
    public List<ToDo> items;
    private View.OnClickListener onCheckBoxClick;

    public TodoListAdapter(@NonNull Context context, @LayoutRes int resource, List<ToDo> Items, View.OnClickListener onCheckBoxClick) {
        super(context, resource);
        this.context = context;
        this.items = Items;
        this.onCheckBoxClick = onCheckBoxClick;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.
                getSystemService(getContext().LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.to_do_list_item, null, false);
        TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        CheckBox cbDone = (CheckBox) row.findViewById(R.id.cbDone);

        cbDone.setOnClickListener(this.onCheckBoxClick);
        tvTitle.setText(" - " + items.get(position).getToDo());
        cbDone.setChecked(items.get(position).isDone());
        cbDone.setTag(position);
        return row;
    }

    @Override
    public int getCount() {
        return items.size();
    }
}
